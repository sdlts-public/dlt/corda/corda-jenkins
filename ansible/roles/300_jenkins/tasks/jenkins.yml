---
- set_fact:
    root_data_folder: "{{ lookup('ansible.builtin.env', 'ROOT_DATA_FOLDER') }}"
    container_type: "{{ lookup('ansible.builtin.env', 'CONTAINER_TYPE') }}"

- name: Resolve mounted folder
  command: "ls {{ root_data_folder }}"
  register: dir_out

- set_fact:
    mounted_folder: "{{ dir_out.stdout }}"

- debug:
    msg: "Found root folder: {{ root_data_folder }}"

- debug:
    msg: "Found folder: {{ mounted_folder }}"

- name: Ensure jenkins folders exist
  file:
    path: "{{ root_data_folder }}/{{ mounted_folder }}/jenkins/{{ item }}"
    state: directory
    recurse: true
  loop:
    - "jenkins_home"
    - "jenkins_share"
    - "jenkins_share/ref"

- name: Copy jenkins casc configuration
  copy:
    src: "files/cfg/{{ item }}"
    dest: "{{ root_data_folder }}/{{ mounted_folder }}/jenkins/jenkins_home/"
    force: yes
  loop:
    - "casc.yaml"

- name: Start jenkins with Docker
  community.docker.docker_container:
    name: jenkins
    image: docker.io/jenkins/jenkins:latest-jdk17
    state: started
    restart_policy: always
    network_mode: host
    docker_host: unix://var/run/docker.sock
    env:
      JAVA_OPTS: "-Djenkins.install.runSetupWizard=false"
      CASC_JENKINS_CONFIG: "{{ lookup('ansible.builtin.env', 'CASC_JENKINS_CONFIG') }}"
      JENKINS_ADMIN_ID: "{{ lookup('ansible.builtin.env', 'JENKINS_ADMIN_ID') }}"
      JENKINS_ADMIN_PASSWORD: "{{ lookup('ansible.builtin.env', 'JENKINS_ADMIN_PASSWORD') }}"
      JENKINS_ADMIN_EMAIL: "{{ lookup('ansible.builtin.env', 'JENKINS_ADMIN_EMAIL') }}"
      JENKINS_HOST_NAME: "{{ lookup('ansible.builtin.env', 'JENKINS_HOST_NAME') }}"
      SECURITY_SCHEME: "{{ lookup('ansible.builtin.env', 'SECURITY_SCHEME') }}"
      CORDA_MANAGER_VAULT_TOKEN_1: "{{ lookup('ansible.builtin.env', 'CORDA_MANAGER_VAULT_TOKEN_1') }}"
      CORDA_MANAGER_VAULT_TOKEN_2: "{{ lookup('ansible.builtin.env', 'CORDA_MANAGER_VAULT_TOKEN_2') }}"
      CORDA_MANAGER_VAULT_TOKEN_3: "{{ lookup('ansible.builtin.env', 'CORDA_MANAGER_VAULT_TOKEN_3') }}"
      CORDA_WEB_CONSOLE_USERNAME_1: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_USERNAME_1') }}"
      CORDA_WEB_CONSOLE_PASSWORD_1: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_PASSWORD_1') }}"
      CORDA_WEB_CONSOLE_USERNAME_2: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_USERNAME_2') }}"
      CORDA_WEB_CONSOLE_PASSWORD_2: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_PASSWORD_2') }}"
      CORDA_WEB_CONSOLE_USERNAME_3: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_USERNAME_3') }}"
      CORDA_WEB_CONSOLE_PASSWORD_3: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_PASSWORD_3') }}"
      CERTIFICATE_PATH_CLIENT_1: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_1') }}"
      CERTIFICATE_PATH_CLIENT_SECRET_1: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_SECRET_1') }}"
      CERTIFICATE_PATH_CA_1: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CA_1') }}"
      CERTIFICATE_PATH_CLIENT_2: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_2') }}"
      CERTIFICATE_PATH_CLIENT_SECRET_2: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_SECRET_2') }}"
      CERTIFICATE_PATH_CA_2: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CA_2') }}"
      CERTIFICATE_PATH_CLIENT_3: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_3') }}"
      CERTIFICATE_PATH_CLIENT_SECRET_3: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_SECRET_3') }}"
      CERTIFICATE_PATH_CA_3: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CA_3') }}"

    volumes:
      - "{{ root_data_folder }}/{{ mounted_folder }}/jenkins/jenkins_home:/var/jenkins_home:Z"
  when: container_type == "docker"

- name: Start jenkins with Podman
  containers.podman.podman_container:
    name: jenkins
    image: docker.io/jenkins/jenkins:latest-jdk17
    state: started
    restart_policy: always
    network_mode: host
    env:
      JAVA_OPTS: "-Djenkins.install.runSetupWizard=false"
      CASC_JENKINS_CONFIG: "{{ lookup('ansible.builtin.env', 'CASC_JENKINS_CONFIG') }}"
      JENKINS_ADMIN_ID: "{{ lookup('ansible.builtin.env', 'JENKINS_ADMIN_ID') }}"
      JENKINS_ADMIN_PASSWORD: "{{ lookup('ansible.builtin.env', 'JENKINS_ADMIN_PASSWORD') }}"
      JENKINS_ADMIN_EMAIL: "{{ lookup('ansible.builtin.env', 'JENKINS_ADMIN_EMAIL') }}"
      JENKINS_HOST_NAME: "{{ lookup('ansible.builtin.env', 'JENKINS_HOST_NAME') }}"
      SECURITY_SCHEME: "{{ lookup('ansible.builtin.env', 'SECURITY_SCHEME') }}"
      VAULT_TOKEN: "{{ lookup('ansible.builtin.env', 'VAULT_TOKEN') }}"
      CORDA_MANAGER_VAULT_TOKEN_1: "{{ lookup('ansible.builtin.env', 'CORDA_MANAGER_VAULT_TOKEN_1') }}"
      CORDA_MANAGER_VAULT_TOKEN_2: "{{ lookup('ansible.builtin.env', 'CORDA_MANAGER_VAULT_TOKEN_2') }}"
      CORDA_MANAGER_VAULT_TOKEN_3: "{{ lookup('ansible.builtin.env', 'CORDA_MANAGER_VAULT_TOKEN_3') }}"
      CORDA_WEB_CONSOLE_USERNAME_1: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_USERNAME_1') }}"
      CORDA_WEB_CONSOLE_PASSWORD_1: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_PASSWORD_1') }}"
      CORDA_WEB_CONSOLE_USERNAME_2: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_USERNAME_2') }}"
      CORDA_WEB_CONSOLE_PASSWORD_2: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_PASSWORD_2') }}"
      CORDA_WEB_CONSOLE_USERNAME_3: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_USERNAME_3') }}"
      CORDA_WEB_CONSOLE_PASSWORD_3: "{{ lookup('ansible.builtin.env', 'CORDA_WEB_CONSOLE_PASSWORD_3') }}"
      CERTIFICATE_PATH_CLIENT_1: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_1') }}"
      CERTIFICATE_PATH_CLIENT_SECRET_1: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_SECRET_1') }}"
      CERTIFICATE_PATH_CA_1: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CA_1') }}"
      CERTIFICATE_PATH_CLIENT_2: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_2') }}"
      CERTIFICATE_PATH_CLIENT_SECRET_2: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_SECRET_2') }}"
      CERTIFICATE_PATH_CA_2: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CA_2') }}"
      CERTIFICATE_PATH_CLIENT_3: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_3') }}"
      CERTIFICATE_PATH_CLIENT_SECRET_3: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CLIENT_SECRET_3') }}"
      CERTIFICATE_PATH_CA_3: "{{ lookup('ansible.builtin.env', 'CERTIFICATE_PATH_CA_3') }}"

    volumes:
      - "{{ root_data_folder }}/{{ mounted_folder }}/jenkins/jenkins_home:/var/jenkins_home:Z,U"
  when: container_type == "podman"
