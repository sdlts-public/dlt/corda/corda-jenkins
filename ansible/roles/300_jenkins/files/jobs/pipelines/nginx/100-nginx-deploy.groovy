node('nginx') {{
    stage ('deploy') {{
        script {{
            sh "{container_type} stop nginx || true && {container_type} rm nginx || true"
            sh "{container_type} run -p 80:80 --restart=unless-stopped " +
                    "-e VAULT_INTERNAL_HOST={vault_address} " +
                    "-e NMS_INTERNAL_HOST={nms_internal_host} " +
                    "-e BASE_DOMAIN_NAME={base_domain_name} " +
                    "-v {jenkins_data_config_folder}/infra/nginx/templates:/etc/nginx/templates " +
                    "-v {jenkins_data_config_folder}/infra/nginx/ssl:/etc/nginx/ssl " +
                    "-d --network=host --name=nginx docker.io/nginx:latest"

        }}
    }}
}}
