def resolve(value, value_if_true, default_value) {{
    if (!value?.trim() || value.equalsIgnoreCase("true")) {{
        return value_if_true
    }}
    return default_value
}}

node('nms') {{
    def project_name = "{project_name}"
    def port_number = "{port}"

    def use_sudo = resolve("${{env.USE_SUDO}}", "sudo", "")

    stage ('deploy') {{
        def secrets = [
                [path: "dev/{target_env}/${{project_name}}/corda/nms/nms_credentials", engineVersion: 2, secretValues: [
                        [envVar: 'nms_username', vaultKey: 'username'],
                        [envVar: 'nms_password', vaultKey: 'password']
                ]],
                [path: "dev/{target_env}/${{project_name}}/corda/nms/trust_store_password", engineVersion: 2, secretValues: [
                        [envVar: 'nms_truststore_password', vaultKey: 'password']
                ]]
        ]

        def configuration = [vaultUrl: "{vault_address}",
                             vaultCredentialId: "${{project_name}}-vault-token",
                             engineVersion: 2]

        withVault([configuration: configuration, vaultSecrets: secrets]) {{
            script {{

                sh "${{use_sudo}} {container_type} run --network=host --restart=unless-stopped -d -p ${{port_number}}:${{port_number}} --name=${{project_name}}-nms --env NMS_ROOT_CA_FILE_PATH='' " +
                        "--env NMS_PORT=${{port_number}} " +
                        '--env NMS_AUTH_USERNAME=${{nms_username}} ' +
                        '--env NMS_AUTH_PASSWORD=${{nms_password}} ' +
                        '--env CORDITE_TRUST_STORE_PASSWORD=${{nms_truststore_password}}' +
                        " -v {jenkins_data_run_folder}/${{project_name}}/nms/data:/opt/cordite/certs:Z,U docker.io/cordite/network-map:latest"

            }}
        }}

    }}
}}
