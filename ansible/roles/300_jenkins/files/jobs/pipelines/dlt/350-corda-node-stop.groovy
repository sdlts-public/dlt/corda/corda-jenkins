node("{node_name}") {{
    def name = "{node_name}"
    def project_name = "{project_name}"

    stage ('undeploy') {{
        script {{
            sh "{container_type} stop ${{project_name}}-${{name}}"
        }}
    }}
}}
