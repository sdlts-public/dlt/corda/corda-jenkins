node("{node_name}") {{

    properties([
            buildDiscarder(logRotator(daysToKeepStr: '5', numToKeepStr: '3')),
    ])


    stage("Invoke other jobs") {{
        build job: "200-node-init", wait: true
        build job: "250-node-migrate", wait: true
        build job: "300-node-start", wait: true
    }}
}}
