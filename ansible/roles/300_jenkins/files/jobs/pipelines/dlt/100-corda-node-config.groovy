def resolve(value, value_if_true, default_value) {{
    if (!value?.trim() || value.equalsIgnoreCase("true")) {{
        return value_if_true
    }}
    return default_value
}}

node("{node_name}") {{

    properties([
            buildDiscarder(logRotator(daysToKeepStr: '5', numToKeepStr: '3')),
    ])

    def project_name = "{project_name}"

    def name = "{node_name}"
    def legalEntityAddress = "{legal_entity_name}"
    def suffix = "{suffix}"
    def is_notary = resolve("{is_notary}", "-notary", "")

    if (!suffix?.trim()) {{
        suffix = "001"
    }}
    if (!name?.trim()) {{
        println "Mandatory params are missing, skipping the build..."
        return currentBuild.result = "SUCCESS"
    }}
    if (!legalEntityAddress?.trim()) {{
        println "Mandatory params are missing, skipping the build..."
        return currentBuild.result = "SUCCESS"
    }}

    def container_name = "${{name}}-init"

    def cmd = "config -rp {target_env}/${{project_name}}/corda  -r /opt/corda/nodes -n ${{legalEntityAddress}} " +
        "-v $VAULT_ADDR " +
        "-mn dev -mg globals -p ${{suffix}} " +
        "-nms {nms_host} ${{is_notary}}"

    def docker_options = " --rm -v {jenkins_data_config_folder}/{target_env}/${{project_name}}/dlt:/opt/corda:Z,U "

    println "Node name: ${{name}}, Legal name adress: ${{legalEntityAddress}}"

    stage("Start generic job to populate themselves") {{
        script {{
            withCredentials([[$class: 'VaultTokenCredentialBinding', credentialsId: "${{ project_name }}-vault-token", vaultAddr: "{vault_address}"]]) {{
                script {{
                    sh "{container_type}" + ' run --network=host --env VAULT_TOKEN=$VAULT_TOKEN ' +
                            " --env VAULT_ADDRESS=$VAULT_ADDR ${{docker_options}} " +
                            " --name=${{container_name}} " +
                            "docker.io/sdltspublic/corda_manager:1.1.0 ${{ name }} ${{ cmd }}"
                }}
            }}
        }}
    }}
}}
