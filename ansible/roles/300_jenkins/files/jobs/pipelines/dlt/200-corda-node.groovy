def resolve(value, value_if_true, default_value) {{
    if (!value?.trim() || value.equalsIgnoreCase("true")) {{
        return value_if_true
    }}
    return default_value
}}

node("{node_name}") {{

    properties([
        buildDiscarder(logRotator(daysToKeepStr: '5', numToKeepStr: '3')),
    ])

    def project_name = "{project_name}"
    def name = "{node_name}"

    def cmd = "{job_type}"

    def additional_docker_options = "{docker_options}"

    if (!name?.trim()) {{
        println "Mandatory params are missing, skipping the build..."
        return currentBuild.result = "SUCCESS"
    }}

    def container_name = "-${{cmd}}";


    def docker_options = "${{additional_docker_options}} -v {jenkins_data_run_folder}/${{project_name}}/corda/${{name}}:/opt/corda/nodes/${{name}}:Z,U " +
        " -v {jenkins_data_config_folder}/{target_env}/${{project_name}}/dlt/nodes/${{name}}/node.conf:/opt/corda/nodes/${{name}}/node.conf:ro" +
        " -v {jenkins_data_config_folder}/{target_env}/${{project_name}}/dlt/${{name}}.yaml:/opt/corda/${{name}}.yaml:ro";

    if (cmd.contains("start")) {{
        docker_options = docker_options + " -d --restart=unless-stopped "
        container_name = ""
    }} else {{
        docker_options = docker_options + " --rm "
    }}

    stage("${{cmd}} Node: ${{name}}") {{
        script {{
            withCredentials([[$class: 'VaultTokenCredentialBinding', credentialsId: "${{ project_name }}-vault-token", vaultAddr: "{vault_address}"]]) {{
                script {{
                    sh "{container_type} stop ${{project_name}}-${{name}} || true"
                    sh "{container_type}" + ' run --network=host --env VAULT_TOKEN=$VAULT_TOKEN' +
                            " --env VAULT_ADDRESS=$VAULT_ADDR " +
                            " ${{docker_options}} " +
                            " --name=${{project_name}}-${{name}}${{container_name}} " +
                            "docker.io/sdltspublic/corda_manager:1.1.0 ${{ name }} ${{ cmd }}"
                }}
            }}
        }}
    }}
}}
