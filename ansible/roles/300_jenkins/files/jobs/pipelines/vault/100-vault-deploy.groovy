node('vault') {{

    stage ('deploy') {{
        script {{
            sh "{container_type} stop vault || true && {container_type} rm vault || true"
            sh "{container_type} run -p 8200:8200 " +
                    "-v {jenkins_data_config_folder}/infra/vault/config:/vault/config " +
                    "-v {jenkins_data_run_folder}/vault/file:/vault/file " +
                    "-v {jenkins_data_run_folder}/vault/logs:/vault/logs " +
                    "--cap-add=IPC_LOCK -d --network=host --name=vault --entrypoint vault hashicorp/vault:latest server -config=/vault/config/vault.json"

        }}
    }}
}}
