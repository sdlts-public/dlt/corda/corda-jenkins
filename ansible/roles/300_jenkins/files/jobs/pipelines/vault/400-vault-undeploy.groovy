node('vault') {{
    stage ('undeploy') {{
        script {{
            sh "{container_type} stop vault || true && {container_type} rm vault || true"
        }}
    }}
}}
