include $(HOME)/.secrets/$(ENV)/environment
include $(CONFIG_ROOT)/$(ENV)/.environment

jenkins:
	ansible-playbook ansible/300_jenkins.yml -i $(CONFIG_ROOT)/$(ENV)/host  --extra-vars "$(NODE_VARS)"

delete-jobs:
	jenkins-jobs --conf ~/.secrets/$(ENV)/jenkins_jobs.ini delete-all

#This should be customized to include custom paths
deploy-jobs:
	jenkins-jobs --conf ~/.secrets/$(ENV)/jenkins_jobs.ini update $(CONFIG_ROOT)/$(ENV)/jenkins/jobs:ansible/roles/300_jenkins/files/jobs/builder

redeploy: delete-jobs deploy-jobs

plugins:
	./scripts/install_plugins.sh

update-plugins:
	./scripts/update_plugins.sh
