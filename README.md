

``bash
export ENV=sample
``
### Create and edit secrets and base config ~/.secrets/$(ENV)/environment


```bash
export CONFIG_ROOT=<Folder, where configuration files are stored>
export ROOT_DATA_FOLDER=<Root folder, where runtime files/data will be stored>

#Jenkins values
export JENKINS_ADMIN_ID=<Jenkins username to be created>
export JENKINS_ADMIN_PASSWORD=<Jenkins password to be created>

```


### Create and edit $(CONFIG_ROOT)/$(ENV)/.environment

```bash
export JAVA_HOME=<JDK if needed>

export HOST_ADDRESS=<IP address to be used with nip.io>

export JENKINS_HOST_NAME=http://localhost:8080
export SECURITY_SCHEME=https
```

### Create and edit $(CONFIG_ROOT)/$(ENV)/host

```properties
[jenkins]
localhost ansible_user=<your current username>
```



# Process

## Install jenkins (and create folders)

```bash
make jenkins
```

## Install jenkins plugins

```bash
make plugins
```

## Install jobs

```bash
make jobs
```


