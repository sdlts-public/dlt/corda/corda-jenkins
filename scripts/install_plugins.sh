#!/usr/bin/env bash

JENKINS_CLI=/tmp/jenkins-cli.jar
JENKINS_URL=http://$HOST_ADDRESS:8080/
JENKINS_AUTH=$JENKINS_ADMIN_ID:$JENKINS_ADMIN_PASSWORD

if [ -z "$JENKINS_URL" ]; then
    echo "Need to set environment variable JENKINS_UR."
    exit 1
fi

if [ -z "$JENKINS_AUTH" ]; then
    echo "Need to set environment variable JENKINS_AUTH (format: 'userId:apiToken')."
    exit 1
fi

if [ -f "$JENKINS_CLI" ]
then
	echo "Using $JENKINS_CLI."
else
	wget -O $JENKINS_CLI $JENKINS_URL/jnlpJars/jenkins-cli.jar
fi

input="ansible/roles/300_jenkins/files/ref/plugins.txt"
while read -r line
do
	echo '' | $JAVA_HOME/bin/java -jar $JENKINS_CLI -s $JENKINS_URL -auth $JENKINS_AUTH  install-plugin $line
done < "$input"

$JAVA_HOME/bin/java -jar $JENKINS_CLI -s $JENKINS_URL -auth $JENKINS_AUTH safe-restart

