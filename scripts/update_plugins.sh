#!/usr/bin/env bash

JENKINS_CLI=/tmp/jenkins-cli.jar
JENKINS_URL=http://$HOST_ADDRESS:8080/
JENKINS_AUTH=$JENKINS_ADMIN_ID:$JENKINS_ADMIN_PASSWORD

if [ -z "$JENKINS_URL" ]; then
    echo "Need to set environment variable JENKINS_UR."
    exit 1
fi

if [ -z "$JENKINS_AUTH" ]; then
    echo "Need to set environment variable JENKINS_AUTH (format: 'userId:apiToken')."
    exit 1
fi

if [ -f "$JENKINS_CLI" ]
then
	echo "Using $JENKINS_CLI."
else
	wget -O $JENKINS_CLI $JENKINS_URL/jnlpJars/jenkins-cli.jar
fi

UPDATE_LIST=$($JAVA_HOME/bin/java -jar $JENKINS_CLI -s $JENKINS_URL -auth $JENKINS_AUTH list-plugins | grep -e ')$' | awk '{ print $1 }')
if [ -n "${UPDATE_LIST}" ]; then
        "$JAVA_HOME"/bin/java -jar $JENKINS_CLI -s "$JENKINS_URL" -auth $JENKINS_AUTH install-plugin ${UPDATE_LIST};
fi

"$JAVA_HOME"/bin/java -jar $JENKINS_CLI -s "$JENKINS_URL" -auth "$JENKINS_AUTH" safe-restart

